<?php
class Main extends CI_Controller {

  private $renderParams = array();
  private $key = '';

  function __construct() {
    parent::__construct();

    $this->load->helper('url');

    if(time() < strtotime('2016-12-22 00:00:00')) {
      redirect('http://www.3m.com.tw/');
    }

    if (!isset($_SESSION)) { session_start(); }

    $this->load->library('user_agent');
  }

  function index() {
    $_SESSION['is_mobile'] = ($this->agent->is_mobile()) ? 'mobile' : 'pc';

    $this->load->view('header', $this->renderParams);
    $this->load->view('index', $this->renderParams);
    $this->load->view('footer', $this->renderParams);
  }

  function captcha() {
    header("Content-type:image/png");

    mt_srand((double)microtime()*1000000);

    $authImg = '';

    $str = 'abcdefghijkmnpqrstuvwxyz';

    $l = strlen($str);

    for($i=0; $i<6; $i++){
       $num=rand(0,$l-1);
       $authImg.= $str[$num];
    }

    $_SESSION["authImg__session"] = $authImg;

    $imageWidth = 300; $imageHeight = 100;

    $im = imagecreatetruecolor($imageWidth, $imageHeight) or die("無法建立圖片！");

    $bgColor = imagecolorallocate($im, 255,239,239);

    $Color = imagecolorallocate($im, 255,0,0);

    $gray1 = imagecolorallocate($im, 200,200,200);

    $gray2 = imagecolorallocate($im, 200,200,200);

    imagefill($im,0,0,$bgColor);

    for($i=0; $i<10; $i++){
       imageline($im,rand(0,$imageWidth),rand(0,$imageHeight),
       rand($imageHeight,$imageWidth),rand(0,$imageHeight),$gray1);
    }

    imagettftext($im, 32, 0, 25, 40, $Color, "/opt/www/ashlieworks/event_temp/command-invoice/images/DejaVuSerif-Bold.ttf", $authImg);

    for($i=0;$i<90;$i++){
       imagesetpixel($im, rand()%$imageWidth ,
       rand()%$imageHeight , $gray2);
    }

    imagepng($im);
    imagedestroy($im);
  }

}
