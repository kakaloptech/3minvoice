<?php

class Process extends CI_Controller {

  function __construct() {
    parent::__construct();

    if (!isset($_SESSION)) { session_start(); }

    $this->load->library('user_agent');
    $this->load->model('mysql_model');
    $this->load->model('mysql_vmodel');
  }

  function set_data() { // 寫入資料庫
    if(isset($_SESSION['authImg__session']) && isset($_SESSION['is_mobile'])) {
      if($_SESSION["authImg__session"] === $this->input->post('captcha')) {
        $formData = array(
          'name' => $this->input->post('name'),
          'telephone' => $this->input->post('telephone'),
          'email' => $this->input->post('email'),
          'invoices' => $this->input->post('invoices'),
          'device' => $_SESSION['is_mobile']
        );

        $chkResult = $this->_check_data($formData);

        if($chkResult['status']) {
          if($this->mysql_vmodel->gen_insert_data($formData)) {
            echo json_encode(array('status' => true, 'message' => '資料登錄成功，非常感謝您的參與'));
          }else{
            echo json_encode(array('status' => false, 'message' => '資料登錄失敗'));
          }
        } else {
          echo json_encode($chkResult);
        }
      } else {
        echo json_encode(array('status' => false, 'message' => '驗證碼錯誤'));
      }
    } else {
      echo json_encode(array('status' => false, 'message' => '驗證碼時效過期，請重新整理頁面'));
    }
  }

  private function _check_exist_invoices($data = array()) { // 檢查發票是否重複
    if(empty($data)) {
      return array(
        'status' => false,
        'message' => '資料錯誤'
      );
    }

    $chkResult = $this->mysql_model->get_exist_invoice($data); // 取得已存在的發票資料

    if(!$chkResult['status']) {
      return array(
        'status' => true
      );
    } else {
      $existInvoice = array();
      foreach($chkResult['data'] as $value) { // 組出已存在發票陣列
        $existInvoice[] = $value['invoice'];
      }
      return array(
        'status' => false,
        'message' => '資料有重複，號碼是 ' . implode(', ', $existInvoice)
      );
    }
  }

  private function _check_data($data = array()) { // 檢查資料是否正確
    if(empty($data)) {
      return array(
        'status' => false,
        'message' => '資料錯誤'
      );
    }
    if(empty($data['name'])) {
      return array(
        'status' => false,
        'message' => '姓名不得空白'
      );
    }
    if(empty($data['telephone'])) {
      return array(
        'status' => false,
        'message' => '電話不得空白'
      );
    } else if(!preg_match('/[0]{1}[9]{1}/', $data['telephone'])) {
      return array(
        'status' => false,
        'message' => '請輸入正確電話格式, error -1'
      );
    } else if(strlen($data['telephone']) < 10) {
      return array(
        'status' => false,
        'message' => '請輸入正確電話格式, error -2'
      );
    }
    if(empty($data['email'])) {
      return array(
        'status' => false,
        'message' => '信箱不得空白'
      );
    } else if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
      return array(
        'status' => false,
        'message' => '請輸入正確信箱格式'
      );
    }
    // foreach ($data['invoices'] as $key => $value) {
    //   if (!preg_match('/[A-Z]{2}[0-9]{8}$/', $value)) {
    //     return array(
    //       'status' => false,
    //       'message' => '請輸入正確的發票格式'
    //     );
    //   }
    // }
    if (count($data['invoices']) <= 0) {
      return array(
        'status' => false,
        'message' => '發票欄位不得空白'
      );
    }
    $chkResult = $this->_check_exist_invoices($data['invoices']);
    if(!$chkResult['status']){
      return $chkResult;
    }
    return array(
      'status' => true
    );
  }

}

?>
