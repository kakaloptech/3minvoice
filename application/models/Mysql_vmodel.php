<?php

class Mysql_vmodel extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->load->model('mysql_model');
    $this->load->library('xxtea');
    $this->key = 'F9378775520F3BD9';
  }

  function gen_insert_data($data = array()) {

    foreach($data['invoices'] as $value) {
      $insertData[] = array(
        'name' => $data['name'],
        'telephone' => $data['telephone'],
        'email' => $data['email'],
        'invoice' => $value,
        'device' => $data['device'],
        'visiable' => 1,
        'created_at' => date('Y-m-d H:i:s')
      );
    }

    return $this->mysql_model->set_data($insertData);;
  }

}
