<?php

class Mysql_model extends CI_Model {

  function __construct() {
    parent::__construct();

    $this->load->database();
  }

  function get_exist_invoice($data = array()) {
    $sql = 'SELECT invoice FROM 3minvoice WHERE invoice IN ?';
    $query = $this->db->query($sql, array($data));
    if($query->num_rows() > 0) {
      return array(
        'status' => true,
        'data' => $query->result_array()
      );
    } else {
      return array(
        'status' => false
      );
    }
  }

  function set_data($data = array()) {
    return $this->db->insert_batch('3minvoice', $data);
  }

}


?>
