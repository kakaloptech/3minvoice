<div id="wrapper">
  <div id="top">
    <div class="header_block">
      <div class="logo_block">
        <img src="images/command_logo.png" class="command_logo">
        <a href="javascript:;" class="menu" onclick="addEvent('command-invoice', 'menu')"></a>
        <a href="javascript:void(window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent(location.href)),'_blank','width=550,height=400,top=100,left=50'));" class="fb" onclick="addEvent('command-invoice', 'share')">facebook 分享</a>
      </div>
    </div>
      <div class="slogan_block">
        <img class="pic" src="images/pic.png">
        <img class="slogan" src="images/slogan.png">
        <img class="flower" src="images/flower.png">
        <a href="javascript:;" class="go_login" onclick="addEvent('command-invoice', 'login')"></a>
      </div>
  </div>
  <div id="content">
    <div class="form">
      <img src="images/form_title.png">
      <div class="form_in">
        <img src="images/t_number.png">
        <ul class="number_list">
          <li>
            <input id="number" name="invoices" class="form_number" placeholder="請輸入發票號碼/網路訂單編號">
          </li>
        </ul>
        <div class="num_btn_block">
          <a class="add_btn" href="javascript:;" onclick="add_invoice_item(); addEvent('command-invoice', 'add')"></a>
          <a class="rewrite_btn" href="javascript:;" onclick="reset_invoice(); addEvent('command-invoice', 'again')"></a>
        </div>
        <img src="images/t_name.png">
        <input id="name" name="name" class="form_name" placeholder="請填寫真實姓名確保中獎權益">
        <img src="images/t_mail.png">
        <input id="mail" name="email" class="form_mail" placeholder="請確認填寫確保中獎權益">
        <img src="images/t_phone.png">
        <input id="phone" name="telephone" class="form_phone" placeholder="請確認填寫確保中獎權益">
        <img src="images/t_captcha.png">
        <input id="captcha" name="captcha" class="form_captcha" value="">
        <div class="captcha_in">
          <div class="captcha_img">
            <img src="/event_temp/command-invoice/main/captcha" border="0">
          </div>
          <a class="reload" href="javascript:;" onclick="reload_captcha()"></a>
        </div>
        <div class="agree_block">
          <input type="checkbox" name="agress" class="agree_check">
          <div class="agree_text">
          我已詳閱
          <a href="javascript:;" onclick="addEvent('command-invoice', 'agree')">活動辦法及注意事項</a>
          並同意相關內容</div>
        </div>
        <a class="send_btn" href="javascript:;" onclick="send_data(); addEvent('command-invoice', 'send')"></a>
      </div>
      <img src="images/form_bottom.png">
    </div>
    <div class="rule">
      <img src="images/rule_title.png">
      <img src="images/rule_c01.jpg">
      <img src="images/rule_c02.jpg">
      <img src="images/rule_c03.jpg">
      <img src="images/rule_c04.jpg">
      <img src="images/rule_c05.jpg">
      <img src="images/rule_c06.jpg">
      <img src="images/rule_c07.jpg">
      <img src="images/rule_c08.jpg">
      <img src="images/rule_c09.jpg">
      <img src="images/rule_c10.jpg">
      <img src="images/rule_c11.jpg">
      <img src="images/rule_c12.png">
      <a class="go_top" href="javascript:;" onclick="addEvent('command-invoice', 'top')"></a>
    </div>
  </div>
  <div id="footer">
    <div class="copyright">© 3M 2015. All Rights Reserved.</div>
  </div>
</div>
<div id="jSplash"></div>

<div class="menu_pop">
  <div class="menu_block">
    <div class="menu_close">
      <a href="javascript:;" onclick="addEvent('command-invoice', 'skip')"></a>
    </div>
    <ul class="menu_list">
      <li><a class="m01" href="javascript:;" onclick="addEvent('command-invoice', 'index')"></a></li>
      <li><a class="m02" href="javascript:;" onclick="addEvent('command-invoice', 'invoice')"></a></li>
      <li><a class="m03" href="javascript:;" onclick="addEvent('command-invoice', 'rule')"></a></li>
      <li><a class="m04" href="javascript:;" onclick="alert('敬請期待'); addEvent('command-invoice', 'list')"></a></li>
    </ul>
  </div>
</div>

<div class="inapp_pop">
  <div class="inapp_block">
    <div class="inapp_close">
      <a href="javascript:;"></a>
    </div>
    <div class="inapp"></div>
  </div>
</div>

<div class="sucsess_pop">
  <div class="sucsess_block">
    <div class="sucsess_close">
      <a href="javascript:;" onclick="addEvent('command-invoice', 'success')"></a>
    </div>
    <div class="sucsess"></div>
  </div>
</div>

<script>
  $(function(){
    addPageView('index');
  })
</script>

<script type="text/javascript" src="js/configs.js"></script>
<script type="text/javascript" src="js/fbfns.js"></script>
<script type="text/javascript" src="js/checks.js"></script>
<script type="text/javascript" src="js/ga.js"></script>
<script type="text/javascript" src="js/main.js"></script>
