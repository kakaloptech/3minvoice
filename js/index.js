$(document).ready(function() {
  animIndex();

});

function animIndex() {
  TweenMax.from($('.pic'), 1, {
    alpha: 0,
    delay: 1.4,
    ease: Cubic.easeOut
  });
  TweenMax.from($('.slogan'), 1, {
    alpha: 0,
    delay: 1.8,
    ease: Cubic.easeOut
  });
  TweenMax.from($('.flower'), 1, {
    alpha: 0,
    delay: 2.2,
    ease: Cubic.easeOut
  });
  TweenMax.from($('.go_login'), 1, {
    alpha: 0,
    bottom: '+=38',
    delay: 2.2,
    ease: Cubic.easeOut
  });
}
$(function() {

  $('body').jpreLoader({
    splashID: "#jSplash",
    splashVPos: '28%',
    loaderVPos: '56%',
    autoClose: true
  });

  $(".go_top").click(function() {
    $('html,body').animate({
      scrollTop: 0
    }, 500);
    return false;
  });

  $(".go_login").click(function() {
    $('html,body').animate({
      scrollTop: $('.form').offset().top
    }, 500);
    return false;
  });

  $(".agree_text a").click(function() {
    $('html,body').animate({
      scrollTop: $('.rule').offset().top
    }, 500);
    return false;
  });

  $('.m01').bind("click", function() {
    $('.menu_pop').fadeOut(200);
    $('.menu_block').fadeOut(200);
    $('html,body').animate({
      scrollTop: 0
    }, 500);
    return false;
  });

  $('.m02').bind("click", function() {
    $('.menu_pop').fadeOut(200);
    $('.menu_block').fadeOut(200);
    $('html,body').animate({
      scrollTop: $('.form').offset().top
    }, 500);
    return false;
  });

  $('.m03').bind("click", function() {
    $('.menu_pop').fadeOut(200);
    $('.menu_block').fadeOut(200);
    $('html,body').animate({
      scrollTop: $('.rule').offset().top
    }, 500);
    return false;
  });

  $('.menu').bind("click", function() {
    $('.menu_pop').fadeIn(200);
    $('.menu_block').fadeIn(200);
  });

  $('.menu_close').bind("click", function() {
    $('.menu_pop').fadeOut(200);
    $('.menu_block').fadeOut(200);
  });

  $('.inapp_close').bind("click", function() {
    $('.inapp_pop').fadeOut(200);
    $('.inapp_block').fadeOut(200);
  });

  $('.sucsess_close').bind("click", function() {
    // $('.sucsess_pop').fadeOut(200);
    // $('.sucsess_block').fadeOut(200);
    $(window).scrollTop(0);
    window.location.reload();
  });

});
