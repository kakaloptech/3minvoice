var isSend = true;

var reload_captcha = function() {
  $('.captcha_img img').attr('src','/event_temp/command-invoice/main/captcha');
}

var reset_invoice = function() {
  var html =  '<li>' +
              '  <input id="number" name="invoices" class="form_number" placeholder="請輸入發票號碼/網路訂單編號">' +
              '</li>';
  $('.number_list').html(html);
}

var get_cookie = function(name) {
  var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
  if(arr != null) return unescape(arr[2]); return null;
}

var add_invoice_item = function() {
  var itemCount = $('input[name=invoices]').length;
  if(itemCount < 5) {
    var html =  '<li>' +
                '  <input id="number" name="invoices" class="form_number" placeholder="請輸入發票號碼/網路訂單編號">' +
                '</li>';
    $('.number_list').append(html);
  } else {
    alert('登入發票個數已達單次登入上限');
    return;
  }
}

var get_total_invoices = function() {
  var totalValue = $('input[name=invoices]');
  var invoices = [];
  for(var key in totalValue) {
    var value = totalValue[key].value;
    if(value){
      invoices.push(value.toUpperCase());
    }
  }
  return invoices;
}

var send_data = function() {
  if(isSend) {
    isSend = false;
    var data = {
      name: $('input[name=name]').val(),
      telephone: $('input[name=telephone]').val(),
      email: $('input[name=email]').val(),
      invoices: get_total_invoices(),
      captcha: $('input[name=captcha]').val(),
      eminv01ce: get_cookie('eminv01ce')
    }
    var chkResult = checks.send_data(data);
    if(chkResult['status']) {
      $.post(config.apiUrl + 'process/set_data', data, function(result){
        if(result['status']) {
          $('.sucsess_pop').fadeIn(200);
    	  	$('.sucsess_block').fadeIn(200);
        } else {
          alert(result['message']);
          isSend = true;
          return;
        }
      }, 'json');
    } else {
      alert(chkResult['message']);
      isSend = true;
      return;
    }
  }
}
