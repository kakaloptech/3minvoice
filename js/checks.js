var Checks = function() {};

Checks.prototype.send_data = function(data) {
  if(data['name'] === '') {
    return {
      status: false,
      message: '姓名不得空白'
    }
  }

  if(data['telephone'] === '') {
    return {
      status: false,
      message: '電話不得空白'
    }
  } else if(data['telephone'].length < 10) {
    return {
      status: false,
      message: '請輸入正確電話格式'
    }
  } else if(!/[0]{1}[9]{1}/.test(data['telephone'])) {
    return {
      status: false,
      message: '請輸入正確電話格式'
    }
  }

  if(data['email'] === '') {
    return {
      status: false,
      message: '信箱不得空白'
    }
  } else if (!/.+@.+\.+.[a-zA-Z]{1,4}$/.test(data['email'])) {
    return {
      status: false,
      message: '請輸入正確的email格式'
    };
  }

  // for(var key in data['invoices']) {
  //   var value = data['invoices'][key];
  //   if (!/[A-Z]{2}[0-9]{8}$/.test(value)) {
  //     return {
  //       status: false,
  //       message: '請輸入正確的發票格式'
  //     };
  //   }
  // }

  if(data['invoices'].length <= 0) {
    return {
      status: false,
      message: '發票不得空白'
    };
  }

  if(!$('input[type=checkbox]').prop('checked')) {
    return {
      status: false,
      message: '請確認詳閱活動部分及注意事項'
    };
  }

  return {
    status: true
  }
};

Checks.prototype.chk_tel = function(event) {
  event.value = event.value.replace(/[^0-9]/g, '');
};

Checks.prototype.chk_invoice = function(event) {
  event.value = event.value.replace(/[^A-Za-z0-9]/g, '');
};

var checks = new Checks();
